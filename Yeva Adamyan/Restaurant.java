public class Test {
    public static void main(String[] args) {
        Restaurant r = new Restaurant();
        RestaurantManager rr = RestaurantManager.getInstance();
        r.setAsianrestaurant(true);
        r.setCountOfStuff(20);
        r.setName("CasaSensei");
        r.setYearOfStarting(2017);
        r.setMenu(new String[]{"coffe", "tea", "macTea"});
        Restaurant r1 = new Restaurant();
        r1.setAsianrestaurant(false);
        r1.setCountOfStuff(23);
        r1.setName("Dragon");
        r1.setYearOfStarting(2019);
        r1.setMenu(new String[]{"sushi", "crab"});
        Restaurant r2 = new Restaurant();
        r2.setAsianrestaurant(true);
        r2.setCountOfStuff(12);
        r2.setName("Thaiwine");
        r2.setMenu(new String[]{"Chicken", "SesameChicken"});
        Restaurant[] arrRest = new Restaurant[]{r, r1, r2};
        rr.printinfo(r);
        rr.printmenu(r);
        rr.printrest(new Restaurant[]{r, r1, r2});
        System.out.println();
        int a = rr.countOfStuff(arrRest);
        System.out.println(a);
        System.out.println();
        rr.printInfoOfRest(arrRest);
        System.out.println();
        System.out.println(rr.maxCountOfStuff(arrRest));
        rr.printTheBiggestMneu(arrRest);
        rr.sortOfRestaurant(arrRest);
    }
}

public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    protected String[] menu;
    private boolean Asianrestaurant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsianrestaurant() {
        return Asianrestaurant;
    }

    public void setAsianrestaurant(boolean asianrestaurant) {
        Asianrestaurant = asianrestaurant;
    }

    public void printinfo() {
        System.out.println("name:" + name);
        System.out.println("yearsofstarting:" + yearOfStarting);
        System.out.println("countoffstuff:" + countOfStuff);
        for (int i = 0; i < menu.length; i++) {
            System.out.println(" menu " + menu[i]);
        }
        System.out.println("AsianRestaurant:" + isAsianrestaurant());
    }
}

public class RestaurantManager {
    private static RestaurantManager ourInstance;

    public static RestaurantManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new RestaurantManager();
        }
        return ourInstance;
    }

    private RestaurantManager() {
    }

    public void printinfo(Restaurant R) {
        System.out.println("Menu");
        for (int i = 0; i < R.menu.length; i++) {
            System.out.println(R.menu[i]);
        }
    }

    public void printmenu(Restaurant Y) {
        System.out.print(Y.getName() + " , " + Y.getCountOfStuff() + " , " + Y.getYearOfStarting());
    }

    public void printrest(Restaurant[] restaurants) {
        for (int i = 0; i < restaurants.length; i++) {
            if (!restaurants[i].isAsianrestaurant()) {
                printmenu(restaurants[i]);
            }
        }
    }

    public int countOfStuff(Restaurant[] rest) {
        int count = 0;
        for (Restaurant r : rest) {
            if (r.getCountOfStuff() > 20) {
                count++;
            }
        }
        return count;
    }

    public void printInfoOfRest(Restaurant[] restof) {
        int newRestaurant = restof[0].getYearOfStarting();
        Restaurant x = null;
        for (int i = 0; i < restof.length; i++) {
            if (restof[i].getYearOfStarting() > newRestaurant) {
                newRestaurant = restof[i].getYearOfStarting();
                x = restof[i];
            }
        }
        x.printinfo();
    }

    public String maxCountOfStuff(Restaurant[] restmax) {
        int max = restmax[0].getCountOfStuff();
        Restaurant myRest = null;
        for (int i = 0; i < restmax.length; i++) {
            if (restmax[i].getCountOfStuff() > max) {
                max = restmax[i].getCountOfStuff();
                myRest = restmax[i];
            }
        }
        return myRest.getName();
    }

    public void printTheBiggestMneu(Restaurant[] restbig) {
        int U = restbig[0].getMenu().length;
        int f = 0;
        for (int i = 0; i < restbig.length; i++) {
            if (restbig[i].getMenu().length > U) {
                U = restbig[i].getMenu().length;
            }
        }
        System.out.println(restbig[f].getName());
    }

    public void sortOfRestaurant(Restaurant[] p) {
        boolean b = true;
        Restaurant h;
        while (b) {
            b = false;
            for (int i = 0; i < p.length - 1; i++) {
                if (p[i].getYearOfStarting() > p[i + 1].getYearOfStarting()) {
                    h = p[i];
                    p[i] = p[i + 1];
                    p[i + 1] = h;
                    b = true;
                }
            }
        }
        for (Restaurant r : p) {
            r.printinfo();
        }
    }
}