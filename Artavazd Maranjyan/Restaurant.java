public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    private String[] menu;
    private boolean isAsian;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsian() {
        return isAsian;
    }

    public void setAsian(boolean asian) {
        isAsian = asian;
    }
}

class RestaurantManager {
    private RestaurantManager() {
    }

    private static RestaurantManager manager;

    public static RestaurantManager getManager() {
        if (manager == null) manager = new RestaurantManager();
        return manager;
    }

    public void printMenu(Restaurant restaurant) {
        for (String x : restaurant.getMenu()) {
            System.out.println(x);
        }
    }

    public void info(Restaurant restaurant) {
        System.out.println(restaurant.getName() + "," + restaurant.getCountOfStuff() + "," + restaurant.getYearOfStarting());
    }

    public void printNotAsianRestaurants(Restaurant[] restaurants) {
        for (Restaurant x : restaurants) {
            if (!x.isAsian()) {
                info(x);
            }
        }
    }

    public int countOfRestaurantsHavingMore20Stuff(Restaurant[] restaurants) {
        int count = 0;
        for (Restaurant x : restaurants) {
            if (x.getCountOfStuff() > 20) {
                count++;
            }
        }
        return count;
    }

    public void theNewestRestaurant(Restaurant[] restaurants) {
        Restaurant max = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (max.getYearOfStarting() < restaurants[i].getYearOfStarting()) {
                max = restaurants[i];
            }
        }
        info(max);
    }

    public String nameOfMaxCountOfStuff(Restaurant[] restaurants) {
        Restaurant max = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (max.getCountOfStuff() < restaurants[i].getCountOfStuff()) {
                max = restaurants[i];
            }
        }
        return max.getName();
    }

    public void printNameOfBiggestMenu(Restaurant[] restaurants) {
        Restaurant max = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (max.getMenu().length < restaurants[i].getMenu().length) {
                max = restaurants[i];
            }
        }
        System.out.println(max.getName());
    }

    public void printAcceding(Restaurant[] restaurants) {
        boolean f = true;
        int k = 0;
        while (f) {
            f = false;
            for (int i = 0; i < restaurants.length - 1 - k; i++) {
                if (restaurants[i].getYearOfStarting() > restaurants[i + 1].getYearOfStarting()) {
                    Restaurant r = restaurants[i];
                    restaurants[i] = restaurants[i + 1];
                    restaurants[i + 1] = r;
                    f = true;
                }
            }
            k++;
        }
        for (Restaurant x : restaurants) {
            info(x);
        }
    }

    public void printDescending(Restaurant[] restaurants) {
        boolean f = true;
        int k = 0;
        while (f) {
            f = false;
            for (int i = 0; i < restaurants.length - 1 - k; i++) {
                if (restaurants[i].getCountOfStuff() < restaurants[i + 1].getCountOfStuff()) {
                    Restaurant r = restaurants[i];
                    restaurants[i] = restaurants[i + 1];
                    restaurants[i + 1] = r;
                    f = true;
                }
            }
            k++;
        }
        for (Restaurant x : restaurants) {
            info(x);
        }
    }
}



 class Test {
    public static void main(String[] args) {
        RestaurantManager manager = RestaurantManager.getManager();
        Restaurant restaurant1 = new Restaurant();
        restaurant1.setMenu(new String[]{"Lasagna", "Loaded baked potato", "Spaghetti", "Pizza"});
        restaurant1.setName("Hungry Howie’s");
        restaurant1.setAsian(false);
        restaurant1.setCountOfStuff(55);
        restaurant1.setYearOfStarting(1999);
        manager.printMenu(restaurant1);
        manager.info(restaurant1);
        Restaurant restaurant2 = new Restaurant();
        restaurant2.setName("Lucky Cat");
        restaurant2.setYearOfStarting(2005);
        restaurant2.setCountOfStuff(33);
        restaurant2.setAsian(true);
        restaurant2.setMenu(new String[]{"EDAMAME", "DIM SUM PLATTER", "SPRING ROLLS", "FINGERFOOD PLATTER", "TUNA TARTARE"});
        Restaurant[] restaurants = {restaurant1, restaurant2};
        manager.printNotAsianRestaurants(restaurants);
        System.out.println(manager.countOfRestaurantsHavingMore20Stuff(restaurants));
        manager.theNewestRestaurant(restaurants);
        System.out.println(manager.nameOfMaxCountOfStuff(restaurants));
        manager.printNameOfBiggestMenu(restaurants);
        manager.printAcceding(restaurants);
        manager.printDescending(restaurants);
    }
}

