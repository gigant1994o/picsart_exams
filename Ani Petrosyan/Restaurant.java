public class Test extends Restaurant {
    public static void main(String[] args) {
        Restaurant rest2 = new Restaurant();
        rest2.setName("Parvana");
        rest2.setYearStarting(2005);
        rest2.setMenu(new String[]{"dish1,dish2,dish3 "});
        rest2.setAsian(false);
        rest2.setCountofstaff(50);
        Restaurant rest1 = new Restaurant();
        rest1.setName("Beijing");
        rest1.setYearStarting(2007);
        rest1.setMenu(new String[]{"dish4, dish5, dish6"});
        rest1.setAsian(true);
        rest1.setCountofstaff(30);
        Restaurant rest3 = new Restaurant();
        rest3.setName("KFC");
        rest3.setYearStarting(2010);
        rest3.setMenu(new String[]{"dish7, dish8, dish9"});
        rest3.setAsian(false);
        rest3.setCountofstaff(100);
        Restaurant rest4 = new Restaurant();
        rest4.setName("Dragon");
        rest4.setYearStarting(2017);
        rest4.setMenu(new String[]{"dish10, dish11, dish12"});
        rest4.setAsian(true);
        rest4.setCountofstaff(60);
    }
}

public class Restaurant {
    private String name;
    private int yearStarting;
    private int sumOfStaff;
    private String[] menu;
    private boolean asian;
    private int countofstaff;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearStarting() {
        return yearStarting;
    }

    public void setYearStarting(int yearStarting) {
        this.yearStarting = yearStarting;
    }

    public int getSumOfStaff() {
        return sumOfStaff;
    }

    public void setSumOfStaff(int sumOfStaff) {
        this.sumOfStaff = sumOfStaff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsian() {
        return asian;
    }

    public void setAsian(boolean asian) {
        this.asian = asian;
    }

    public int getCountofstaff() {
        return countofstaff;
    }

    public void setCountofstaff(int countofstaff) {
        this.countofstaff = countofstaff;
    }
}

public class RestaurantManager1 extends Restaurant {
    public void printMenu(Restaurant restaurant) {
        for (String current : restaurant.getMenu()) {
            System.out.println(current);
        }
    }

    public void getInfo(Restaurant restaurant) {
        System.out.println(restaurant.getName() + " " + restaurant.getCountofstaff() + " " + restaurant.getYearStarting());
    }
}