public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    private String[] menu;
    private Boolean isAsian;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsian() {
        return isAsian;
    }

    public void setAsian(boolean asian) {
        isAsian = asian;
    }

    class RestaurantManager {
        private static RestaurantManager ourInstance = new RestaurantManager();

        public static RestaurantManager getInstance() {
            return ourInstance;
        }

        private RestaurantManager() {
        }

        public void printMenu(Restaurant restaurant) {
            for (int i = 0; i < restaurant.getMenu().length; i++) {
                System.out.println(i + 1 + "." + restaurant.getMenu()[i]); // ??????
            }
        }

        //
        public void printInfo(Restaurant restaurant) {
            System.out.println(" name " + restaurant.getName() + ", count of stuff " + restaurant.getCountOfStuff() + ", year " + restaurant.getYearOfStarting());
        }

        public void printNotAsians(Restaurant[] restaurants) {
            for (int i = 0; i < restaurants.length; i++) {
                if (restaurants[i].isAsian()) {
                    System.out.println(restaurants[i].getName());
                }
            }
        }

        public int countofRestaurant(Restaurant[] restaurants) {
            int count = 0;
            for (int i = 0; i < restaurants.length; i++) {
                if (restaurants[i].getCountOfStuff() > 20) {
                    count++;
                }
            }
            return count;
        }

        public void printNewRestaurantInf(Restaurant[] restaurants) {
            Restaurant max = restaurants[0];
            for (int i = 1; i < restaurants.length; i++) {
                if (restaurants[i].getYearOfStarting() > max.getYearOfStarting()) {
                    max = restaurants[i];
                }
            }
            printInfo(max);
        }

        public void printBiggestMenu(Restaurant[] restaurants) {
            Restaurant biggestMenu = restaurants[0];
            for (int i = 1; i < restaurants.length; i++) {
                if (restaurants[i].getMenu().length > biggestMenu.getMenu().length) {
                    biggestMenu = restaurants[i];
                }
            }
            System.out.println(biggestMenu.getName());
        }

        public void printSortedByYear(Restaurant[] restaurants) {
            for (int a = 1; a < restaurants.length; a++) {
                for (int b = 0; b < restaurants.length - a; b++) {
                    if (restaurants[b].getYearOfStarting() > restaurants[b + 1].getYearOfStarting()) {
                        Restaurant temp = restaurants[b];
                        restaurants[b] = restaurants[b + 1];
                        restaurants[b + 1] = temp;
                    }
                }
            }
            for (int a = 0; a < restaurants.length; a++) {
                printInfo(restaurants[a]);
            }
        }

        public void printSortedByStuff(Restaurant[] restaurants) {
            for (int a = 1; a < restaurants.length; a++) {
                for (int b = 0; b < restaurants.length - a; b++) {
                    if (restaurants[b].getCountOfStuff() < restaurants[b + 1].getCountOfStuff()) {
                        Restaurant temp = restaurants[b];
                        restaurants[b] = restaurants[b + 1];
                        restaurants[b + 1] = temp;
                    }
                }
            }
            for (int a = 0; a < restaurants.length; a++) {
                printInfo(restaurants[a]);
            }
        }
    }

    public class Main {
        public static void main(String[] args) {
            RestaurantManager Manager = RestaurantManager.getInstance();
            Restaurant restaurant1 = new Restaurant();
            restaurant1.setName("Pandok");
            restaurant1.setYearOfStarting(2002);
            restaurant1.setAsian(false);
            restaurant1.setCountOfStuff(30);
            restaurant1.setMenu(new String[]{"Spas", "Salad", "Cheese", "Bread"});
            System.out.println("Fist restaurant");
            Manager.printInfo(restaurant1);
            System.out.println("Menu of the first restaurant");
            Manager.printMenu(restaurant1);
            System.out.println("-----------------------");
            Restaurant restaurant2 = new Restaurant();
            restaurant2.setName("Zaituna");
            restaurant2.setYearOfStarting(2009);
            restaurant2.setAsian(true);
            restaurant2.setCountOfStuff(10);
            restaurant2.setMenu(new String[]{"Mutabal", "Sish Taug", "Humus"});
            System.out.println("Second restaurant");
            Manager.printInfo(restaurant2);
            System.out.println("Menu of the second restaurant");
            Manager.printMenu(restaurant2);
            System.out.println("-----------------------");
            System.out.print("Name of the Asian restaurant: ");
            Manager.printNotAsians(new Restaurant[]{restaurant1, restaurant2});
            System.out.print("Count of restaurants haveing 20+ stuff: ");
            System.out.println(Manager.countofRestaurant(new Restaurant[]{restaurant1, restaurant2}));
            System.out.print("The newest restaurant information: ");
            Manager.printNewRestaurantInf(new Restaurant[]{restaurant1, restaurant2});
            System.out.print("The name of the restaurant having the biggest Menu: ");
            Manager.printBiggestMenu(new Restaurant[]{restaurant1, restaurant2});
            System.out.print("Information of the restaurants sorted by year in acceding form: ");
            Manager.printSortedByYear(new Restaurant[]{restaurant1, restaurant2});
            System.out.print("Information of the restaurants sorted by count of staff in: ");
            Manager.printSortedByStuff(new Restaurant[]{restaurant1, restaurant2});
        }
    }
}