class RestaurantManager {
    private RestaurantManager() {
        System.out.println("Hi! I am cheff Gustav I'll be your manager!");
    }

    private static RestaurantManager manager = null;

    public static RestaurantManager getInstace() {
        if (manager == null) {
            manager = new RestaurantManager();
            return manager;
        }
        return manager;
    }

    public void printMenu(Restaurant restaurant) {
        restaurant.printRestaurantMenu();
    }

    public void printInfo(Restaurant restaurant) {
        restaurant.printRestaurantInfo();
    }

    public void nonAsianInfo(Restaurant[] restaurants) {
        System.out.println("List of non Asian Restaurants");
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].nonAsian()) restaurants[i].printRestaurantInfo();
        }
    }

    public int staffMoreThanTwenty(Restaurant[] restaurants) {
        System.out.print("Number of restaurants with staff more than twenty: ");
        int counter = 0;
        for (int i = 0; i < restaurants.length; i++) if (restaurants[i].getCountOfStuff() > 20) counter++;
        return counter;
    }

    public void newerRestaurantInfo(Restaurant[] restaurants) {
        int index = 0, compare = restaurants[0].getYearOfStart();
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getYearOfStart() > compare) {
                compare = restaurants[i].getYearOfStart();
                index = i;
            }
        }
        restaurants[index].printRestaurantInfo();
    }

    public String maxStaffName(Restaurant[] restaurants) {
        int compare = restaurants[0].getCountOfStuff();
        String name = restaurants[0].getName();
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCountOfStuff() > compare) {
                name = restaurants[i].getName();
                compare = restaurants[i].getCountOfStuff();
            }
        }
        return name;
    }

    public void biggestMenu(Restaurant[] restaurants) {
        int compare = restaurants[0].getMenu().length;
        String name = restaurants[0].getName();
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getMenu().length > compare) {
                compare = restaurants[i].getMenu().length;
                name = restaurants[i].getName();
            }
        }
        System.out.println(name);
    }

    public void restaurantsAccending(Restaurant[] restaurants) {
        Restaurant temp = new Restaurant("temp", 1980, 20, new String[]{"temp", "temp"}, true);
        for (int i = 0; i < restaurants.length - 1; i++) {
            for (int j = 1; j < restaurants.length - 1 - i; j++) {
                if (restaurants[j].getYearOfStart() < restaurants[j + 1].getYearOfStart()) {
                    temp = restaurants[j];
                    restaurants[j] = restaurants[j + 1];
                    restaurants[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < restaurants.length; i++) {
            restaurants[i].printRestaurantInfo();
        }
        System.out.println("*********************");
    }

    public void restaurantsDescending(Restaurant[] restaurants) {
        Restaurant temp = new Restaurant("temp", 1980, 20, new String[]{"temp", "temp"}, true);
        for (int i = 0; i < restaurants.length - 1; i++) {
            for (int j = 0; j < restaurants.length - 1 - i; j++) {
                if (restaurants[j].getCountOfStuff() > restaurants[j + 1].getCountOfStuff()) {
                    temp = restaurants[j];
                    restaurants[j] = restaurants[j + 1];
                    restaurants[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < restaurants.length; i++) {
            restaurants[i].printRestaurantInfo();
        }
        System.out.println("*********************");
    }
}

class Restaurant {
    private String name;
    private int yearOfStart;
    private int countOfStuff;
    private String[] menu;
    private boolean asian;

    public Restaurant(String name, int yearOfStart, int countOfStuff, String[] menu, boolean asian) {
        setName(name);
        setYearOfStart(yearOfStart);
        setCountOfStuff(countOfStuff);
        setMenu(menu);
        setAsian(asian);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStart() {
        return yearOfStart;
    }

    public void setYearOfStart(int yearOfStart) {
        if (yearOfStart < 0 || yearOfStart > 2019) this.yearOfStart = 2000;
        this.yearOfStart = yearOfStart;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        if (countOfStuff < 0) this.countOfStuff = 20;
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsian() {
        return asian;
    }

    public void setAsian(boolean asian) {
        this.asian = asian;
    }

    public void printRestaurantMenu() {
        System.out.println("Todays menu is!");
        for (int i = 0; i < menu.length; i++) {
            System.out.println(i + 1 + ": " + this.menu[i]);
        }
    }

    public void printRestaurantInfo() {
        System.out.printf("Name: %s \nNumber of Stuff: %d \nOrigin year: %d \n", this.getName(), this.getCountOfStuff(), this.getYearOfStart());
    }

    public Boolean nonAsian() {
        return !this.isAsian();
    }
}

class Test {
    public static void main(String[] args) {
        RestaurantManager manager = RestaurantManager.getInstace();
        Restaurant artash = new Restaurant("Artashi mot", 1998, 60, new String[]{"BBQ", "Kebab", "Tan", "Shaurma"}, false);
        Restaurant foo = new Restaurant("Foo Chao", 1960, 14, new String[]{"Sushi", "Camel hump", "Shishini", "Karri", "Ramen", "Octopus legs"}, true);
        Restaurant lafe = new Restaurant("La Petit Marcel", 2018, 26, new String[]{"Lozanya", "Crouassanie", "Pasta with Shato-bordo", "Boulanje", "ShokManje", "Jejeje"}, false);
        Restaurant hajime = new Restaurant("Hajime", 1973, 10, new String[]{"Fugu", "Octopus", "Rice with soya", "Sake", "Geja"}, true);
        Restaurant york = new Restaurant("New York Pizza", 1997, 55, new String[]{"Pizza-pepperoni", "Pizza-americano", "Pizza with ham", "Vegan pizza", "Spicy pizza"}, false);
        Restaurant[] array = {artash, foo, lafe, hajime, york};
        for (int i = 0; i < array.length; i++) {
            array[i].printRestaurantInfo();
            System.out.println("123123123123");
        }
        manager.printMenu(artash);
        manager.printInfo(foo);
        manager.biggestMenu(array);
        manager.maxStaffName(array);
        System.out.println("Restaurant with max stuff is: " + manager.maxStaffName(array));
        manager.nonAsianInfo(array);
        System.out.println("There are " + manager.staffMoreThanTwenty(array) + " restaurants with stuff more than twenty");
        manager.newerRestaurantInfo(array);
        manager.restaurantsAccending(array);
        manager.restaurantsDescending(array);
    }
}


