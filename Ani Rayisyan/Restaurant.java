public class Main {
    public static void main(String[] args) {
        Restaurant r1 = new Restaurant();
        r1.setName("Dragon garden");
        r1.setYearOfStarting(2000);
        r1.setCountOfStuff(50);
        r1.setMenu(new String[]{"Roll", "Sushi", "cactus's tea"});
        r1.setAsina(true);
        Restaurant r2 = new Restaurant();
        r2.setName("Italiano");
        r2.setYearOfStarting(2005);
        r2.setCountOfStuff(17);
        r2.setMenu(new String[]{"Spagetti", "Bolonez", "Vine"});
        r2.setAsina(false);
        Restaurant r3 = new Restaurant();
        r3.setName("Pizza de France");
        r3.setYearOfStarting(1995);
        r3.setCountOfStuff(48);
        r3.setMenu(new String[]{"Assorti", "Diablo", "Margarita", "Coca cola", "Pepsi"});
        r3.setAsina(false);
        Restaurant r4 = new Restaurant();
        r4.setName("Beijing");
        r4.setYearOfStarting(2015);
        r4.setCountOfStuff(30);
        r4.setMenu(new String[]{"Yan", "Kinoyu", "White vine", "Tyun-shu"});
        r4.setAsina(true);
        RestaurantManager manager = new RestaurantManager();
        manager.menuOfRestaurant(r1, r2, r3, r4);
        System.out.println("----------------------------");
        manager.informationAboutRestaurant(r1, r2, r3, r4);
        System.out.println("----------------------------");
        manager.notAsianRestaurant(new Restaurant[]{r1, r2, r3, r4});
        System.out.println("------------------------------");
        System.out.println(manager.bigStuff(new Restaurant[]{r1, r2, r3, r4}));
        ;
        System.out.println("--------------------------------");
        manager.newerRestautant(new Restaurant[]{r1, r2, r3, r4});
        System.out.println("----------------------------------");
        System.out.println(manager.maxCountOfStaff(new Restaurant[]{r1, r2, r3, r4}));
        System.out.println("-----------------------------------");
        manager.biggestMenu(new Restaurant[]{r1, r2, r3, r4});
        System.out.println("-----------------------------------");
        manager.sorted(new Restaurant[]{r1, r2, r3, r4});
    }
}

public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    private boolean isAsina;
    private String[] menu;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public boolean isAsina() {
        return isAsina;
    }

    public void setAsina(boolean asina) {
        isAsina = asina;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public void printInfo() {
        System.out.println("Name:" + name);
        System.out.println("year of starting:" + yearOfStarting);
        System.out.println("count of stuff:" + countOfStuff);
        System.out.println("menu:" + menu);
        System.out.println("is Asian restaurant or not.:" + isAsina);
    }
} import java.awt.*;

public class RestaurantManager { //1. parameter: restaurant , result : print menu of restaurant
    public void menuOfRestaurant(Restaurant r1, Restaurant r2, Restaurant r3, Restaurant r4) {
        System.out.println(r1.getMenu());
        System.out.println(r1.getMenu());
    } //2. parameter: restaurant, result : info in this format "name, count of stuff, year"

    public void informationAboutRestaurant(Restaurant r1, Restaurant r2, Restaurant r3, Restaurant r4) {
        System.out.println(r1.getName() + "," + r1.getCountOfStuff() + "," + r1.getYearOfStarting());
        System.out.println(r2.getName() + "," + r2.getCountOfStuff() + "," + r2.getYearOfStarting());
        System.out.println(r3.getName() + "," + r3.getCountOfStuff() + "," + r3.getYearOfStarting());
        System.out.println(r4.getName() + "," + r4.getCountOfStuff() + "," + r4.getYearOfStarting());
    } //3. parameter: array of restaurants, result: print not Asian restaurants

    public void notAsianRestaurant(Restaurant[] restaurants) {
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].isAsina() == false) {
                System.out.println(restaurants[i].getName());
            }
        }
    } //3. parameter: array of restaurants, result: return count of restaurants having 20+ stuff

    public int bigStuff(Restaurant[] restaurants) {
        int count = 0;
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCountOfStuff() > 20) {
                count++;
            }
        }
        return count;
    } //4. parameter: array of restaurants, result: print the newer restaurant information

    public void newerRestautant(Restaurant[] restaurants) {
        int newOne = restaurants[0].getYearOfStarting();
        int max = 0;
        for (int i = 1; i < restaurants.length; i++) {
            if (restaurants[i].getYearOfStarting() > newOne) {
                newOne = restaurants[i].getYearOfStarting();
                max = i;
            }
        }
        restaurants[max].printInfo();
    } //5. parameter: array of restaurants, result: return the name of the restaurant having the maximal count of stuff

    public String maxCountOfStaff(Restaurant[] restaurants) {
        int countMax = restaurants[0].getCountOfStuff();
        int rest = 0;
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCountOfStuff() > countMax) {
                countMax = restaurants[i].getCountOfStuff();
                rest = i;
            }
        }
        return restaurants[rest].getName();
    } //6. parameter: array of restaurants, result: print the name of the restaurant having the biggest Menu

    public void biggestMenu(Restaurant[] restaurants) {
        int j = restaurants[0].getMenu().length;
        int n = 0;
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getMenu().length > j) {
                j = restaurants[i].getMenu().length;
                n = i;
            }
        }
        System.out.println(restaurants[n].getName());
    } //7. parameter: array of restaurants, result: print information of the restaurants sorted by year in acceding form

    public void sorted(Restaurant[] restaurants) {
        boolean isSorted = true;
        Restaurant temp;
        while (isSorted) {
            isSorted = false;
            for (int i = 0; i < restaurants.length - 1; i++) {
                if (restaurants[i].getYearOfStarting() > restaurants[i + 1].getYearOfStarting()) {
                    temp = restaurants[i];
                    restaurants[i] = restaurants[i + 1];
                    restaurants[i + 1] = temp;
                    isSorted = true;
                }
            }
        }
        for (int i = 0; i < restaurants.length; i++) {
            System.out.print(restaurants[i].getName() + ",");
        }
    }
}