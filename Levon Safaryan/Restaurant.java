public class Test {
    public static void main(String[] args) {
        Restaurant r2 = new Restaurant();
        r2.getMenu();
        Restaurant r3 = new Restaurant();
        r3.setName("JOSE");
        r3.setCountOfStaff(25);
        r3.setYearOfStarting(2000);
        r3.setMenu(new String[]{"meat", "bread", "wine", "fish"});
        r3.setIsAsianRestaurantOrNot(true);
        Restaurant r4 = new Restaurant();
        r4.setName("Parvana");
        r4.setCountOfStaff(30);
        r4.setYearOfStarting(1995);
        r4.setMenu(new String[]{"meat", "bread", "wine", "water", "coffee"});
        r4.setIsAsianRestaurantOrNot(true);
        Restaurant r5 = new Restaurant();
        r5.setName("Lusabac");
        r5.setCountOfStaff(15);
        r5.setYearOfStarting(2007);
        r5.setMenu(new String[]{"meat", "wine"});
        r5.setIsAsianRestaurantOrNot(false);
        Restaurant r6 = new Restaurant();
        r6.setName("Virginia");
        r6.setCountOfStaff(10);
        r6.setYearOfStarting(2015);
        r6.setMenu(new String[]{"meat", "bread", "wine"});
        r6.setIsAsianRestaurantOrNot(false);
        Restaurant[] restaurant = {r3, r4, r5, r6};
        RestaurantManager manager = RestaurantManager.getInstance();
        //1 manager.restaurantMenu(r3);
        // 2 manager.restaurantInfo(r3);
        // 3 manager.asianRestaurants(restaurant);
        // 4 manager.countOfRestaurants(restaurant);
        // 5 manager.newerRestaurant(restaurant);
        // 6 manager.maximalCountOfStaff(restaurant);
        // 7 manager.biggestMenu(restaurant);
        // 8 manager.sortedByAccedingForm(restaurant);
        // 9 manager.sortedByDescendingForm(restaurant);
    }
}

public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStaff;
    private String[] menu;
    private boolean isAsianRestaurantOrNot;

    public boolean getIsAsianRestaurantOrNot() {
        return isAsianRestaurantOrNot;
    }

    public void setIsAsianRestaurantOrNot(boolean isAsianRestaurantOrNot) {
        this.isAsianRestaurantOrNot = isAsianRestaurantOrNot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStaff() {
        return countOfStaff;
    }

    public void setCountOfStaff(int countOfStaff) {
        this.countOfStaff = countOfStaff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public void printInfo() {
        System.out.println("Name" + " " + name);
        System.out.println("Menu" + " " + Arrays.toString(menu));
        System.out.println("Count of staff" + " " + countOfStaff);
        System.out.println("Year of starting" + " " + yearOfStarting);
        System.out.println("Is Asian restaurant" + " " + (isAsianRestaurantOrNot ? "Yes" : "No"));
    }
}

public class RestaurantManager extends Restaurant {
    private static RestaurantManager ourInstance = new RestaurantManager();

    public static RestaurantManager getInstance() {
        return ourInstance;
    }

    private RestaurantManager() {
    }

    Restaurant r1 = new Restaurant(); //1 public void restaurantMenu(Restaurant a) { System.out.println(Arrays.toString(a.getMenu())); } //2 public void restaurantInfo(Restaurant b) { System.out.println(b.getName() + " " + b.getCountOfStaff() + " " + b.getYearOfStarting()); } //3 public void asianRestaurants(Restaurant[] c) { for (int i = 0; i < c.length; i++) { if (c[i].getIsAsianRestaurantOrNot() == false) { System.out.println("Not Asian restaurants are" + " " + c[i].getName()); } } } //4 public int countOfRestaurants(Restaurant[] d) { int count = 0; for (int i = 0; i < d.length; i++) { if (d[i].getCountOfStaff() > 20) { count++; } } return count; } //5 public void newerRestaurant(Restaurant[] e) { Restaurant max = e[0]; for (int i = 0; i < e.length; i++) { if (e[i].getYearOfStarting() > max.getYearOfStarting()) { max = e[i]; } } max.printInfo(); } //6 public String maximalCountOfStaff(Restaurant[] f) { Restaurant max = f[0]; for (int i = 0; i < f.length; i++) { if (f[i].getCountOfStaff() > max.getCountOfStaff()) { max = f[i]; } } return max.getName(); } //7 public void biggestMenu(Restaurant[] g) { Restaurant max = g[0]; for (int i = 0; i < g.length; i++) { if (g[i].getMenu().length > max.getMenu().length) { max = g[i]; } } System.out.println(max.getName()); } //8 public void sortedByAccedingForm(Restaurant[] h) { Restaurant max = h[0]; for (int i = 0; i < h.length; i++) { if (h[i].getYearOfStarting() > max.getYearOfStarting()) { max = h[i]; } } max.printInfo(); } //9 public void sortedByDescendingForm(Restaurant[] n) { Restaurant min = n[0]; for (int i = 0; i < n.length; i++) { if (n[i].getCountOfStaff() < min.getCountOfStaff()) { min = n[i]; } } min.printInfo(); } }