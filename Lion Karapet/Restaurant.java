public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    protected String[] menu;
    private boolean AsianRestaurant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsianRestaurant() {
        return AsianRestaurant;
    }

    public void setAsianRestaurant(boolean asianRestaurant) {
        AsianRestaurant = asianRestaurant;
    }
} package Restaurant_Manager_Exam;

public class RestaurantManager {
    private static RestaurantManager ourInstance;

    public static RestaurantManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new RestaurantManager();
        }
        return ourInstance;
    }

    private RestaurantManager() {
    } //functions

    public void printMenu(Restaurant restaurant) {
        System.out.println("--------------------Restaurant menu-----------------");
        for (int i = 0; i < restaurant.menu.length; i++) {
            System.out.println(restaurant.menu[i]);
        }
    }

    public void printInfo(Restaurant restaurant) {
        System.out.println("-----------------------Restaurant info-------------------------------");
        System.out.println(restaurant.getName() + ", " + restaurant.getCountOfStuff() + ", " + restaurant.getYearOfStarting());
    }

    public void printNotAsianRestaurants(Restaurant[] restaurants) {
        System.out.println("------------------Not Asian Restaurants----------------");
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].isAsianRestaurant() == false) {
                printInfo(restaurants[i]);
            }
        }
    }

    public int countOfRestaurantsHavingMoreThan20Stuff(Restaurant[] restaurants) {
        System.out.println("--------------------Number of Restaurants that has more than 20 stuff-----------------------");
        int count = 0;
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCountOfStuff() > 20) {
                count++;
            }
        }
        return count;
    }

    public void nameOfRestaurantHavingMaximalCountOFStuff(Restaurant[] restaurants) {
        System.out.println("-----------------name Of Restaurant Having Maximal Count OF Stuff is:");
        Restaurant r = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (r.getCountOfStuff() < restaurants[i].getCountOfStuff()) {
                r = restaurants[i];
            }
        }
        System.out.println(r.getName());
    }

    public void newerRestaurantInfo(Restaurant[] restaurants) {
        System.out.println("------------newer Restaurant Info---------------");
        Restaurant r = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (r.getYearOfStarting() < restaurants[i].getYearOfStarting()) {
                r = restaurants[i];
            }
        }
        printInfo(r);
    }

    public void nameOfRestaurantHavingBiggestMenu(Restaurant[] restaurants) {
        System.out.println("----------------name Of Restaurant Having Biggest Menu is: ");
        Restaurant r = restaurants[0];
        for (int i = 1; i < restaurants.length; i++) {
            if (r.menu.length < restaurants[i].menu.length) {
                r = restaurants[i];
            }
        }
        System.out.println(r.getName());
    }

    public void infoOfRestaurantsInAccendingOrderByYear(Restaurant[] restaurants) {
        System.out.println("--------------sorting by year-----------------");
        int countOfFors = 0;
        System.out.println("----------UNSORTED----------");
        for (int i = 0; i < restaurants.length; i++) {
            System.out.println(restaurants[i].getName() + ", " + restaurants[i].getCountOfStuff() + ", " + restaurants[i].getYearOfStarting());
        }
        boolean enteredIf = true;
        while (enteredIf) {
            enteredIf = false;
            for (int i = 0; i < restaurants.length - 1 - countOfFors; i++) {
                if (restaurants[i].getYearOfStarting() < restaurants[i + 1].getYearOfStarting()) {
                    Restaurant temp = restaurants[i];
                    restaurants[i] = restaurants[i + 1];
                    restaurants[i + 1] = temp;
                    enteredIf = true;
                }
            }
            countOfFors++;
        }
        System.out.println("----------SORTED----------");
        for (int i = 0; i < restaurants.length; i++) {
            System.out.println(restaurants[i].getName() + ", " + restaurants[i].getCountOfStuff() + ", " + restaurants[i].getYearOfStarting());
        }
    }

    public void infoOfRestaurantsSortedByCountOfStuff(Restaurant[] restaurants) {
        System.out.println("--------------sorting by count of stuff-----------------");
        int countOfFors = 0;
        System.out.println("----------UNSORTED----------");
        for (int i = 0; i < restaurants.length; i++) {
            System.out.println(restaurants[i].getName() + ", " + restaurants[i].getCountOfStuff() + ", " + restaurants[i].getYearOfStarting());
        }
        boolean enteredIf = true;
        while (enteredIf) {
            enteredIf = false;
            for (int i = 0; i < restaurants.length - 1 - countOfFors; i++) {
                if (restaurants[i].getCountOfStuff() < restaurants[i + 1].getCountOfStuff()) {
                    Restaurant temp = restaurants[i];
                    restaurants[i] = restaurants[i + 1];
                    restaurants[i + 1] = temp;
                    enteredIf = true;
                }
            }
            countOfFors++;
        }
        System.out.println("----------SORTED----------");
        for (int i = 0; i < restaurants.length; i++) {
            System.out.println(restaurants[i].getName() + ", " + restaurants[i].getCountOfStuff() + ", " + restaurants[i].getYearOfStarting());
        }
    }
}

public class Test {
    public static void main(String[] args) {
        RestaurantManager restaurantManager = RestaurantManager.getInstance();
        Restaurant restaurant1 = new Restaurant();
        restaurant1.setName("Pandok Yerevan");
        restaurant1.setYearOfStarting(2014);
        restaurant1.setCountOfStuff(20);
        restaurant1.setMenu(new String[]{"Khorovats", "Spas", "Havi Qyabab"});
        restaurant1.setAsianRestaurant(false);
        Restaurant restaurant2 = new Restaurant();
        restaurant2.setName("Beijing Asian Restaurant");
        restaurant2.setYearOfStarting(2006);
        restaurant2.setCountOfStuff(25);
        restaurant2.setMenu(new String[]{"Sushi", "Philadelphia", "Green river", "Rice"});
        restaurant2.setAsianRestaurant(true);
        Restaurant restaurant3 = new Restaurant();
        restaurant3.setName("Dragon Garden Restaurant");
        restaurant3.setYearOfStarting(2010);
        restaurant3.setCountOfStuff(30);
        restaurant3.setMenu(new String[]{"Sushi", "Philadelphia", "Green river"});
        restaurant3.setAsianRestaurant(true);
        restaurantManager.printMenu(restaurant1);
        System.out.println();
        restaurantManager.printInfo(restaurant1);
        System.out.println();
        restaurantManager.printNotAsianRestaurants(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
        System.out.println(restaurantManager.countOfRestaurantsHavingMoreThan20Stuff(new Restaurant[]{restaurant1, restaurant2, restaurant3}));
        System.out.println();
        restaurantManager.newerRestaurantInfo(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
        restaurantManager.nameOfRestaurantHavingMaximalCountOFStuff(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
        restaurantManager.nameOfRestaurantHavingBiggestMenu(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
        restaurantManager.infoOfRestaurantsInAccendingOrderByYear(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
        restaurantManager.infoOfRestaurantsSortedByCountOfStuff(new Restaurant[]{restaurant1, restaurant2, restaurant3});
        System.out.println();
    }
}