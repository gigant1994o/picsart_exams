public class Restaurant {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStaff() {
        return countOfStaff;
    }

    public void setCountOfStaff(int countOfStaff) {
        this.countOfStaff = countOfStaff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public Boolean getAsean() {
        return isAsean;
    }

    public void setAsean(Boolean asean) {
        isAsean = asean;
    }

    private String name;
    private int yearOfStarting;
    private int countOfStaff;
    private String[] menu;
    private Boolean isAsean;
}

public class RestaurantManager extends Restaurant {
    public void PrintInfo() {
        System.out.println("name: " + getName());
        System.out.println("year of starting: " + getYearOfStarting());
        System.out.println("is Asian: " + (getAsean() ? "Yes" : "No"));
    }

    public void PrintMenu() {
        for (int i = 0; i <= getMenu().length; i++) {
            System.out.println("menu: " + getMenu()[i]);
        }
    }

    public int PrintRestaurant(Restaurant[] restaurants) {
        int count = 0;
        for (int i = 0; i <= restaurants.length; i++) {
            if (getCountOfStaff() > 20) {
                count++;
                System.out.println(restaurants[i].length());
            }
        }
        return count;
    }

    public void PrintNewRestInfo(Restaurant a, Restaurant b) {
        if (a.getYearOfStarting() > b.getYearOfStarting()) {
            System.out.println(a.getYearOfStarting());
        } else {
            System.out.println(b.getYearOfStarting());
        }
    }

    public Restaurant maxCountOfStuff(Restaurant[] restaurants) {
        Restaurant max = restaurants[0];
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCountOfStaff() > max.getCountOfStaff()) {
                max = restaurants[i];
            }
        }
        return max;
    }

    public void notAsian(Restaurant[] restaurants) {
        for (int i = 0; i <= restaurants.length; i++) {
            if (!restaurants[i].getAsean()) {
                System.out.println(restaurants[i]);
            }
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Restaurant r1 = new Restaurant();
        r1.setAsean(true);
        r1.setCountOfStaff(25);
        r1.setName("pyxk");
        Restaurant r2 = new Restaurant();
        r2.setAsean(true);
        r2.setName("shyxk");
        r2.setCountOfStaff(30);
        Restaurant[] restaurants = {r1, r2};
        RestaurantManager manager = new RestaurantManager();
        manager.maxCountOfStuff(restaurants);
        manager.PrintNewRestInfo(r1, r2);
        manager.PrintRestaurant(restaurants);
        manager.PrintMenu();
        manager.notAsian(restaurants);
    }
}