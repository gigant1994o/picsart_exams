public class Test {
    public static void main(String[] args) {
        Restaurant r1 = new Restaurant();
        r1.setName("Armenia");
        r1.setYearOfStarting(2000);
        r1.setCountOfStuff(30);
        r1.setMenu(new String[]{"pizaa", "spas", "xapama"});
        r1.setAsianRestaurantOrNot(false);
        Restaurant r2 = new Restaurant();
        r2.setName("Catpuding");
        r2.setYearOfStarting(2004);
        r2.setCountOfStuff(10);
        r2.setMenu(new String[]{"buter", "hotdog", "chessburger", "burger", "chessburger"});
        r2.setAsianRestaurantOrNot(true);
        Restaurant r3 = new Restaurant();
        r3.setName("NewYorq");
        r3.setYearOfStarting(2002);
        r3.setCountOfStuff(35);
        r3.setMenu(new String[]{"fish", "soup", "puding", "sendvich"});
        r3.setAsianRestaurantOrNot(false);
        RestaurntManager rm = RestaurntManager.getInstance();
        Restaurant[] arrayOfRestaurant = {r1, r2, r3};
        rm.restaurantMenu(r1);
        System.out.println("1-------------------------");
        rm.info(r1);
        System.out.println("2-------------------------");
        rm.notAsianRestaurant(arrayOfRestaurant);
        System.out.println("3-------------------------");
        System.out.println(rm.stuffRestaurant(arrayOfRestaurant));
        System.out.println("4-------------------------");
        rm.newrestautantinfo(arrayOfRestaurant);
        System.out.println("5-------------------------");
        System.out.println(rm.maxStuffRestaurant(arrayOfRestaurant));
        System.out.println("6-------------------------");
        rm.maxMenuRestaurant(arrayOfRestaurant);
        System.out.println("7-------------------------");
        rm.sortYearRestaurant(arrayOfRestaurant);
        System.out.println("8-------------------------");
        rm.sortCountRestaurant(arrayOfRestaurant);
        System.out.println("9-------------------------");
    }
} //---------------------------------------

public class RestaurntManager {
    private static RestaurntManager ourInstance = new RestaurntManager();

    public static RestaurntManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new RestaurntManager();
        }
        return ourInstance;
    }

    private RestaurntManager() {
    }

    public void restaurantMenu(Restaurant a) {
        int k = 1;
        for (String s : a.getMenu()) {
            System.out.println(k++ + ": " + s);
        }
    }

    public void info(Restaurant a) {
        System.out.println(a.getName() + "," + a.getCountOfStuff() + "," + a.getYearOfStarting());
    }

    public void notAsianRestaurant(Restaurant[] a) {
        for (Restaurant r : a) {
            if (r.isAsianRestaurantOrNot()) {
            } else {
                System.out.println(r.getName());
            }
        }
    }

    public int stuffRestaurant(Restaurant[] a) {
        int k = 0;
        for (Restaurant r : a) {
            if (r.getCountOfStuff() > 20) {
                k++;
            }
        }
        return k;
    }

    public void newrestautantinfo(Restaurant[] a) {
        int k = a[0].getYearOfStarting();
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i].getYearOfStarting() > k) {
                k = a[i].getYearOfStarting();
                j = i;
            }
        }
        a[j].printInfo();
    }

    public int maxStuffRestaurant(Restaurant[] a) {
        int k = a[0].getCountOfStuff();
        for (int i = 0; i < a.length; i++) {
            if (a[i].getCountOfStuff() > k) {
                k = a[i].getCountOfStuff();
            }
        }
        return k;
    }

    public void maxMenuRestaurant(Restaurant[] a) {
        int k = a[0].getMenu().length;
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i].getMenu().length > k) {
                k = a[i].getMenu().length;
                j = i;
            }
        }
        System.out.println(a[j].getName());
    }

    public void sortYearRestaurant(Restaurant[] a) {
        boolean bol = true;
        Restaurant h;
        while (bol) {
            bol = false;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i].getYearOfStarting() > a[i + 1].getYearOfStarting()) {
                    h = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = h;
                    bol = true;
                }
            }
        }
        for (Restaurant r : a) {
            System.out.println(r.getName());
        }
    }

    public void sortCountRestaurant(Restaurant[] a) {
        boolean bol = true;
        Restaurant h;
        while (bol) {
            bol = false;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i].getCountOfStuff() < a[i + 1].getCountOfStuff()) {
                    h = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = h;
                    bol = true;
                }
            }
        }
        for (Restaurant r : a) {
            r.printInfo();
        }
    }
} //-----------------------------

public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStuff;
    private String[] menu;
    private boolean isAsianRestaurantOrNot;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return countOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.countOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsianRestaurantOrNot() {
        return isAsianRestaurantOrNot;
    }

    public void setAsianRestaurantOrNot(boolean asianRestaurantOrNot) {
        isAsianRestaurantOrNot = asianRestaurantOrNot;
    }

    public void printInfo() {
        int k = 1;
        System.out.println("name: " + name);
        System.out.println("YearOfStarting: " + yearOfStarting);
        System.out.println("CountOfStuff: " + countOfStuff);
        for (int i = 0; i < menu.length; i++) {
            System.out.println(k++ + " menu: " + menu[i]);
        }
        System.out.println("isAsianRestaurantOrNot: " + isAsianRestaurantOrNot);
    }
}