public class Restaurant {
    private String name;
    private int yearOfStarting;
    private int countOfStaff;
    private String[] menu;
    private boolean isAsianOrNote;

    public Restaurant() {
    }

    public Restaurant(String name, int yearOfStarting, int countOfStaff, boolean isAsianOrNote) {
        this.name = name;
        this.yearOfStarting = yearOfStarting;
        this.countOfStaff = countOfStaff;
        this.isAsianOrNote = isAsianOrNote;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStarting() {
        return yearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        this.yearOfStarting = yearOfStarting;
    }

    public int getCountOfStaff() {
        return countOfStaff;
    }

    public void setCountOfStaff(int countOfStaff) {
        this.countOfStaff = countOfStaff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isAsianOrNote() {
        return isAsianOrNote;
    }

    public String asianOrNote() {
        if (isAsianOrNote) {
            return "Yes";
        } else {
            return "No";
        }
    }

    public void setAsianOrNote(boolean asianOrNote) {
        isAsianOrNote = asianOrNote;
    }

    public void infoPrint() {
        System.out.println("Name : " + getName());
        System.out.println("Starting in : " + getYearOfStarting());
        System.out.println("Staff : " + getCountOfStaff());
        System.out.println("Asian or Not : " + asianOrNote());
    }
}

public class RestaurantManager {
    private static RestaurantManager myManager = null;

    private RestaurantManager() {
    }

    public static RestaurantManager manager() {
        if (myManager == null) {
            return myManager = new RestaurantManager();
        }
        return myManager;
    }

    public void printMenuOfRestaurant(Restaurant restoran) {
        int rLength = restoran.getMenu().length;
        for (int i = 0; i < rLength; i++) {
            System.out.println(restoran.getMenu()[i]);
        }
    }

    public void printInfo(Restaurant restoran) {
        System.out.println("Restoran Name: " + restoran.getName() + " Count of Staff: " + restoran.getCountOfStaff() + " Year of Start: " + restoran.getYearOfStarting());
    }

    public void printNotAsian(Restaurant[] restoran) {
        for (Restaurant res : restoran) {
            if (!res.isAsianOrNote()) {
                System.out.println(res.getName());
            }
        }
    }

    public int countOfStaff(Restaurant[] restoran) {
        int count = 0;
        for (Restaurant res : restoran) {
            if (res.getCountOfStaff() > 20) {
                count++;
            }
        }
        return count;
    }

    public void printRestaurantInfo(Restaurant[] restoran) {
        int newer = restoran[0].getYearOfStarting();
        Restaurant newRest = null;
        for (int i = 1; i < restoran.length; i++) {
            if (restoran[i].getYearOfStarting() > newer) {
                newer = restoran[i].getYearOfStarting();
                newRest = restoran[i];
            }
        }
        newRest.infoPrint();
    }

    public String printMaximalStaffCount(Restaurant[] restoran) {
        int maxCount = restoran[0].getCountOfStaff();
        Restaurant newRest = null;
        for (int i = 1; i < restoran.length; i++) {
            if (restoran[i].getCountOfStaff() > maxCount) {
                maxCount = restoran[i].getCountOfStaff();
                newRest = restoran[i];
            }
        }
        return newRest.getName();
    }

    public String biggestMenu(Restaurant[] restoran) {
        int bigMenu = restoran[0].getMenu().length;
        Restaurant newRest = null;
        for (Restaurant res : restoran) {
            if (res.getMenu().length > bigMenu) {
                bigMenu = res.getMenu().length;
                newRest = res;
            }
        }
        return newRest.getName();
    }

    public void printSortingByYear(Restaurant[] restoran) {
        for (int i = 0; i < restoran.length - 1; i++) {
            for (int j = 0; j < restoran.length - i - 1; j++) {
                if (restoran[j].getYearOfStarting() > restoran[j - 1].getYearOfStarting()) {
                    Restaurant res = restoran[j];
                    restoran[j] = restoran[j + 1];
                    restoran[j + 1] = res;
                }
            }
        }
        for (Restaurant res : restoran) {
            res.infoPrint();
            System.out.println("_____________________________________");
        }
    }

    public void printSortingByStaffCount(Restaurant[] restoran) {
        for (int i = 0; i < restoran.length - 1; i++) {
            for (int j = 0; j < restoran.length - i - 1; j++) {
                if (restoran[j].getCountOfStaff() < restoran[j + 1].getCountOfStaff()) {
                    Restaurant res = restoran[j];
                    restoran[j] = restoran[j + 1];
                    restoran[j + 1] = res;
                }
            }
        }
        for (Restaurant res : restoran) {
            res.infoPrint();
            System.out.println("_____________________________________");
        }
    }
}

public class Test {
    public static void main(String[] args) {
        Restaurant restoran1 = new Restaurant("Cascade", 2015, 15, true);
        Restaurant restoran2 = new Restaurant("Paulaner", 2006, 19, true);
        Restaurant restoran3 = new Restaurant("Maloco", 2018, 26, false);
        Restaurant restoran4 = new Restaurant("Floor", 2010, 22, false);
        Restaurant[] arrayOfRest = new Restaurant[]{restoran1, restoran2, restoran3, restoran4};
        RestaurantManager myManager = RestaurantManager.manager(); //
        // myManager.printMenuOfRestaurant(myRestaurant);
        // myManager.printInfo(restoran1);
        // System.out.println("3 question");
        // System.out.println();
        // myManager.printNotAsian(arrayOfRest);
        // System.out.println("3 question");
        // System.out.println();
        // System.out.println(myManager.countOfStaff(arrayOfRest));
        // System.out.println("4 question");
        // System.out.println();
        // System.out.println(myManager.countOfStaff(arrayOfRest));
        // System.out.println();
        // System.out.println("4 question");
        // System.out.println(); //
        // myManager.printRestaurantInfo(arrayOfRest);
        // System.out.println();
        // System.out.println("5 question");
        // System.out.println(); //
        // System.out.println(myManager.printMaximalStaffCount(arrayOfRest)); //
        // System.out.println();
        // System.out.println("6 question");
        // System.out.println(); //
        // // System.out.println(myManager.biggestMenu(arrayOfRest)); //
        // // // System.out.println();
        // System.out.println("7 question");
        // System.out.println();
        myManager.printSortingByStaffCount(arrayOfRest);

    }
}