public class Restaurant {
    private String Name;
    private int YearOfStarting;
    private int CountOfStuff;
    private String[] Menu;
    private boolean Assianres;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getYearOfStarting() {
        return YearOfStarting;
    }

    public void setYearOfStarting(int yearOfStarting) {
        YearOfStarting = yearOfStarting;
    }

    public int getCountOfStuff() {
        return CountOfStuff;
    }

    public void setCountOfStuff(int countOfStuff) {
        this.CountOfStuff = countOfStuff;
    }

    public String[] getMenu() {
        return Menu;
    }

    public void setMenu(String[] menu) {
        Menu = menu;
    }

    public boolean isAssianres() {
        return Assianres;
    }

    public void setAssianres(boolean assianres) {
        Assianres = assianres;
    }

    public void infoPrint() {
        System.out.println(getName());
        System.out.println(getYearOfStarting());
    }
}

public class RestaurantManager<v> {
    private static RestaurantManager ourInstance = new RestaurantManager();

    public static RestaurantManager getInstance() {
        return ourInstance;
    }

    public static RestaurantManager getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(RestaurantManager ourInstance) {
        RestaurantManager.ourInstance = ourInstance;
    }

    private RestaurantManager() {
    } // Task 01

    public void PrintMenu(Restaurant menu) {
        for (int i = 0; i < menu.getMenu().length; i++) {
            System.out.println(i + 1 + ". " + menu.getMenu()[i]);
        }
    } //Task 02

    public void printInfo(Restaurant restaurant) {
        System.out.print(restaurant.getName() + "," + restaurant.getCountOfStuff() + "," + restaurant.getYearOfStarting());
    } //Task 03

    public void asianORnotePrint(Restaurant[] rest) {
        for (Restaurant r : rest) {
            if (!r.isAssianres()) {
                r.infoPrint();
            }
        }
    } //Task 04

    public int stuffnumber(Restaurant[] rest) {
        int count = 0;
        for (int i = 0; i < rest.length; i++) {
            if (rest[i].getCountOfStuff() > 20) {
                count++;
            }
        }
        return count;
    }

    public void newerrestaurant(Restaurant[] rest) {
        Restaurant myRest = null;
        int maxYear = rest[0].getYearOfStarting();
        for (int i = 0; i < rest.length; i++) {
            if (rest[i].getYearOfStarting() > maxYear) {
                maxYear = rest[i].getYearOfStarting();
                myRest = rest[i];
            }
        }
        myRest.infoPrint();
    } //Task 06

    public String maxStaffCount(Restaurant[] rest) {
        Restaurant myRest = null;
        int maxStuff = rest[0].getCountOfStuff();
        for (int i = 0; i < rest.length; i++) {
            if (rest[i].getCountOfStuff() > maxStuff) {
                maxStuff = rest[i].getCountOfStuff();
                myRest = rest[i];
            }
        }
        return myRest.getName();
    } //Taks 07

    public String biggestMenu(Restaurant[] rest) {
        Restaurant myRest = null;
        int bigmenu = rest[0].getMenu().length;
        for (int i = 0; i < rest.length; i++) {
            if (rest[i].getMenu().length > bigmenu) {
                bigmenu = rest[i].getMenu().length;
                myRest = rest[i];
            }
        }
        return myRest.getName();
    } //Task 08

    public void sortedByYear(Restaurant[] rest) {
        for (int i = 0; i < rest.length; i++) {
            for (int j = 1; j < rest.length; j++) {
                if (rest[j - 1].getYearOfStarting() > rest[j].getYearOfStarting()) {
                    Restaurant x = rest[j - 1];
                    rest[j - 1] = rest[j];
                    rest[j] = x;
                }
            }
        }
        for (Restaurant r : rest) {
            System.out.println(r.getName());
        }
    } //Task 09

    public void sortedByStaff(Restaurant[] rest) {
        for (int i = 0; i < rest.length; i++) {
            for (int j = 1; j < rest.length; j++) {
                if (rest[j - 1].getCountOfStuff() < rest[j].getCountOfStuff()) {
                    Restaurant x = rest[j - 1];
                    rest[j - 1] = rest[j];
                    rest[j] = x;
                }
            }
        }
        for (Restaurant r : rest) {
            System.out.println(r.getName());
        }
    }
}

public class test {
    public static void main(String[] args) {
        RestaurantManager restaurantManager = RestaurantManager.getInstance();
        Restaurant r1 = new Restaurant();
        r1.setName("Namaste");
        r1.setYearOfStarting(2010);
        r1.setCountOfStuff(10);
        r1.setMenu(new String[]{"Food1", "Food2", "Food3"});
        r1.setAssianres(true);
        Restaurant r2 = new Restaurant();
        r2.setName("Asiano");
        r2.setYearOfStarting(2015);
        r2.setCountOfStuff(30);
        r2.setMenu(new String[]{"food1", "food2", "food3", "drink1", "drink2"});
        r2.setAssianres(false);
        Restaurant r3 = new Restaurant();
        r3.setName("Asialand");
        r3.setYearOfStarting(2019);
        r3.setCountOfStuff(20);
        r3.setMenu(new String[]{"fd1", "fd2", "fo3", "drk1", "drk2", "drk3"});
        r3.setAssianres(true);
        Restaurant[] arrayRestaurant = new Restaurant[]{r1, r2, r3};
        System.out.println("-----------Task 01-----------");
        System.out.println("..Menu..");
        restaurantManager.PrintMenu(r1);
        System.out.println();
        System.out.println("-----------Task 02-----------");
        restaurantManager.printInfo(r1);
        System.out.println();
        System.out.println("-----------Task 03-----------");
        restaurantManager.asianORnotePrint(arrayRestaurant);
        System.out.println();
        System.out.println("-----------Task 04-----------");
        System.out.println(restaurantManager.stuffnumber(arrayRestaurant));
        System.out.println();
        System.out.println("-----------Task 05-----------");
        restaurantManager.newerrestaurant(arrayRestaurant);
        System.out.println();
        System.out.println("-----------Task 06-----------");
        System.out.println(restaurantManager.maxStaffCount(arrayRestaurant));
        System.out.println();
        System.out.println("-----------Task 07-----------");
        System.out.println(restaurantManager.biggestMenu(arrayRestaurant));
        System.out.println();
        System.out.println("-----------Task 08-----------");
        restaurantManager.sortedByYear(arrayRestaurant);
        System.out.println();
        System.out.println("-----------Task 09-----------");
        restaurantManager.sortedByStaff(arrayRestaurant);
        System.out.println();
    }
}