public class Restaurant {
    public int getYear_of_starting() {
        return year_of_starting;
    }

    public void setYear_of_starting(int year_of_starting) {
        this.year_of_starting = year_of_starting;
    }

    public int getCount_of_staff() {
        return count_of_staff;
    }

    public void setCount_of_staff(int count_of_staff) {
        this.count_of_staff = count_of_staff;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public boolean isIs_assian_restaurant_or_not() {
        return is_assian_restaurant_or_not;
    }

    public void setIs_assian_restaurant_or_not(boolean is_assian_restaurant_or_not) {
        this.is_assian_restaurant_or_not = is_assian_restaurant_or_not;
    }

    private int year_of_starting;
    private int count_of_staff;
    private String[] menu;
    private boolean is_assian_restaurant_or_not;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
} // RestaurantManager

public class RestaurantManager extends Restaurant {
    //1
    public void PrintMenu(String[] menu) {
        for (int i = 0; i <= menu.length; i++) {
            System.out.println("menu:" + menu[i]);
        }
    } // 2

    public void PrintInfo(String name, int yearOsStart, Boolean Asian) {
        System.out.println("name:" + name);
        System.out.println("Start Year" + yearOsStart);
        System.out.println("Asian:" + (Asian ? "Yes" : "No"));
    } //4

    public void PrintNew(Restaurant x, Restaurant y) {
        if (x.getYear_of_starting() > y.getYear_of_starting()) {
            System.out.println(x.getYear_of_starting());
        } else {
            System.out.println(y.getYear_of_starting());
        }
    } // 5

    public Restaurant maxStaffCount(Restaurant[] restaurants) {
        Restaurant max = restaurants[0];
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i].getCount_of_staff() > max.getCount_of_staff()) {
                max = restaurants[i];
            }
        }
        return max;
    } //3

    public int PrintRestaurant(String[] restaurants, int staff) {
        int countofStaff = 0;
        for (int i = 0; i <= restaurants.length; i++) {
            if (staff > 20) {
                countofStaff++;
                System.out.println(restaurants[i].length());
            }
        }
        return countofStaff;
    }
} // Test

public class Test {
    public static void main(String[] args) {
        Restaurant rest11 = new Restaurant();
        rest11.setIs_assian_restaurant_or_not(true);
        rest11.setCount_of_staff(10);
        rest11.setName("Florence");
        Restaurant rest2 = new Restaurant();
        rest2.setIs_assian_restaurant_or_not(true);
        rest2.setName("Hilton");
        rest2.setCount_of_staff(90);
        Restaurant[] restaurants = {rest11, rest2};
        RestaurantManager manager = new RestaurantManager();
        manager.maxStaffCount(restaurants);
        manager.PrintNew(rest11, rest2);
        manager.PrintRestaurant(restaurants);
        manager.PrintMenu();
    }
}